<?php
/**
 * My Company Helpdesk System
 * Copyright (C) 2020 My Company
 *
 * This file is part of Mycompany/Helpdesk.
 *
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Controller\Adminhtml\Ticket;

use Magento\Framework\Exception\LocalizedException;
use Mycompany\Helpdesk\Api\Data\TicketInterface;

class Save extends \Magento\Backend\App\Action
{

    /**
     * @var \Magento\Framework\App\Request\DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Mycompany\Helpdesk\Model\Ticket
     */
    protected $ticket;

    /**
     * @var \Mycompany\Helpdesk\Model\Thread
     */
    protected $thread;


    /**
     * Save constructor.
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Mycompany\Helpdesk\Model\Ticket $ticket
     * @param \Mycompany\Helpdesk\Model\Thread $thread
     * @param \Mycompany\Helpdesk\Model\ThreadRepository $threadRepository
     * @param \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
     */
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Mycompany\Helpdesk\Model\Ticket $ticket,
        \Mycompany\Helpdesk\Model\Thread $thread,
        \Mycompany\Helpdesk\Model\ThreadRepository $threadRepository,
        \Magento\Framework\App\Request\DataPersistorInterface $dataPersistor
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->thread = $thread;
        $this->ticket = $ticket;

        parent::__construct($context);
    }

    /**
     * Save action
     *
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        $data = $this->getRequest()->getPostValue();
        $attachment = null;

        if ($data) {

            try {

                // prepare the ticket
                $ticket = $this->ticket->prepare($data);

                // assign the ticket Id to the thread
                $data['general']['ticket_id'] = $ticket->getTicketId();

                // check if there is an attachment
                if(!empty($data['general']['attachment'][0])){
                    $attachment = $data['general']['attachment'][0];
                }

                // prepare the thread registration
                $this->thread->prepare($ticket->getTicketId(), $data['general']['message'], $data['options']['reply_date'], $attachment, $data['options']['parent_id'], $data['furtherinfo']['generated_by'], $data['options']['status_id'], $data['options']['notification']);

                $this->messageManager->addSuccessMessage(__('You saved the Ticket.'));
                $this->dataPersistor->clear('mycompany_helpdesk_ticket');

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', ['ticket_id' => $ticket->getTicketId()]);
                }
                return $resultRedirect->setPath('*/*/');

            } catch (\LocalizedException $e) {
                $this->messageManager->addErrorMessage($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addExceptionMessage($e, __('Something went wrong while saving the Ticket.'));
            }

            $this->dataPersistor->set('mycompany_helpdesk_ticket', $data);
            return $resultRedirect->setPath('*/*/edit', ['ticket_id' => $data['general']['ticket_id']]);
        }

        return $resultRedirect->setPath('*/*/');
    }
}
