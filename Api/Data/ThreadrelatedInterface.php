<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Api\Data;

/**
 * Interface ThreadrelatedInterface
 *
 * @package Mycompany\Helpdesk\Api\Data
 */
interface ThreadrelatedInterface extends \Magento\Framework\Api\ExtensibleDataInterface
{

    const THREAD_ID = 'thread_id';
    const THREADRELATED_ID = 'threadrelated_id';
    const TICKET_ID = 'ticket_id';
    const REFERENCE_ID = 'reference_id';

    /**
     * Get threadrelated_id
     * @return string|null
     */
    public function getThreadrelatedId();

    /**
     * Set threadrelated_id
     * @param string $threadrelatedId
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface
     */
    public function setThreadrelatedId($threadrelatedId);

    /**
     * Get ticket_id
     * @return string|null
     */
    public function getTicketId();

    /**
     * Set ticket_id
     * @param string $ticketId
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface
     */
    public function setTicketId($ticketId);

    /**
     * Get reference_id
     * @return string|null
     */
    public function getReferenceId();

    /**
     * Set reference_id
     * @param string $referenceId
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface
     */
    public function setReferenceId($referenceId);

    /**
     * Retrieve existing extension attributes object or create a new one.
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedExtensionInterface|null
     */
    public function getExtensionAttributes();

    /**
     * Set an extension attributes object.
     * @param \Mycompany\Helpdesk\Api\Data\ThreadrelatedExtensionInterface $extensionAttributes
     * @return $this
     */
    public function setExtensionAttributes(
        \Mycompany\Helpdesk\Api\Data\ThreadrelatedExtensionInterface $extensionAttributes
    );

    /**
     * Get thread_id
     * @return string|null
     */
    public function getThreadId();

    /**
     * Set thread_id
     * @param string $threadId
     * @return \Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface
     */
    public function setThreadId($threadId);
}

