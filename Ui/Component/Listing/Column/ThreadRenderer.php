<?php
namespace Mycompany\Helpdesk\Ui\Component\Listing\Column;

use Magento\Framework\Escaper;
use Magento\Framework\View\Element\UiComponent\ContextInterface;
use Magento\Framework\View\Element\UiComponentFactory;
use Magento\Ui\Component\Listing\Columns\Column;

/**
 * Get the records from an external table
 */
class ThreadRenderer extends Column
{
    /**
     * Escaper
     *
     * @var \Magento\Framework\Escaper
     */
    protected $escaper;

    /**
     * System store
     *
     * @var SystemStore
     */
    protected $systemStore;

    /**
     * @var \Mycompany\Helpdesk\Model\ThreadFactory
     */
    protected $records;

    /**
     * ThreadRenderer constructor.
     * @param ContextInterface $context
     * @param UiComponentFactory $uiComponentFactory
     * @param Escaper $escaper
     * @param \Mycompany\Helpdesk\Model\ThreadFactory $factory
     * @param array $components
     * @param array $data
     */
    public function __construct(
        ContextInterface $context,
        UiComponentFactory $uiComponentFactory,
        Escaper $escaper,
        \Mycompany\Helpdesk\Model\ThreadFactory $factory,
        array $components = [],
        array $data = []
    ) {
        $this->records = $factory;
        $this->escaper = $escaper;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            foreach ($dataSource['data']['items'] as & $item) {
                $collection = $this->records->create()->getCollection();
                $collection->addFieldToFilter('ticket_id', ['eq' => $item['ticket_id']]);
                $counter = $collection->count();
                if($counter > 5) {
                    $item[$this->getData('name')] = html_entity_decode('<span style="font-weight:bold;color: darkred">' . $counter . "</span>");
                }else{
                    $item[$this->getData('name')] = html_entity_decode('<span style="font-weight:bold;color: black">' . $counter . "</span>");
                }
            }
        }

        return $dataSource;
    }
}