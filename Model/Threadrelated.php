<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Model;

use Magento\Framework\Api\DataObjectHelper;
use Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface;
use Mycompany\Helpdesk\Api\Data\ThreadrelatedInterfaceFactory;

/**
 * Class Threadrelated
 *
 * @package Mycompany\Helpdesk\Model
 */
class Threadrelated extends \Magento\Framework\Model\AbstractModel
{

    protected $dataObjectHelper;

    protected $_eventPrefix = 'mycompany_helpdesk_threadrelated';
    protected $threadrelatedDataFactory;


    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param ThreadrelatedInterfaceFactory $threadrelatedDataFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param \Mycompany\Helpdesk\Model\ResourceModel\Threadrelated $resource
     * @param \Mycompany\Helpdesk\Model\ResourceModel\Threadrelated\Collection $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        ThreadrelatedInterfaceFactory $threadrelatedDataFactory,
        DataObjectHelper $dataObjectHelper,
        \Mycompany\Helpdesk\Model\ResourceModel\Threadrelated $resource,
        \Mycompany\Helpdesk\Model\ResourceModel\Threadrelated\Collection $resourceCollection,
        array $data = []
    ) {
        $this->threadrelatedDataFactory = $threadrelatedDataFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve threadrelated model with threadrelated data
     * @return ThreadrelatedInterface
     */
    public function getDataModel()
    {
        $threadrelatedData = $this->getData();
        
        $threadrelatedDataObject = $this->threadrelatedDataFactory->create();
        $this->dataObjectHelper->populateWithArray(
            $threadrelatedDataObject,
            $threadrelatedData,
            ThreadrelatedInterface::class
        );
        
        return $threadrelatedDataObject;
    }
}

