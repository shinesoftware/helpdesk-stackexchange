<?php

namespace Mycompany\Helpdesk\Model\ResourceModel\Threadrelated\Grid;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\Search\SearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\App\RequestInterface as RequestInterfaceAlias;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Mycompany\Helpdesk\Api\Data\ThreadrelatedInterfaceFactory;
use Mycompany\Helpdesk\Model\ResourceModel\Threadrelated;
use Mycompany\Helpdesk\Model\ResourceModel\Threadrelated\Collection as ThreadrelatedCollection;
use Mycompany\Helpdesk\Model\Threadrelated as MainCollection;

class Collection extends MainCollection implements SearchResultInterface
{
    protected $aggregations;
    protected $request;

    public function __construct(Context $context,
                                Registry $registry,
                                ThreadrelatedInterfaceFactory $threadrelatedDataFactory,
                                DataObjectHelper $dataObjectHelper,
                                Threadrelated $resource,
                                ThreadrelatedCollection $resourceCollection,
                                RequestInterfaceAlias $request,
                                array $data = [])
    {
        parent::__construct($context, $registry, $threadrelatedDataFactory, $dataObjectHelper, $resource, $resourceCollection, $data);

        $this->request = $request;
    }

    public function getAggregations()
    {
        return $this->aggregations;
    }

    public function setAggregations($aggregations)
    {
        $this->aggregations = $aggregations;
    }

    public function getAllIds($limit = null, $offset = null)
    {
        return $this->getConnection()->fetchCol($this->_getAllIdsSelect($limit, $offset), $this->_bindParams);
    }

    public function getSearchCriteria()
    {
        return null;
    }

    public function setSearchCriteria(SearchCriteriaInterface $searchCriteria = null)
    {
        return $this;
    }

    public function getTotalCount()
    {
        return $this->getSize();
    }

    public function setTotalCount($totalCount)
    {
        return $this;
    }

    public function setItems(array $items = null)
    {
        return $this;
    }

    // this function filter your grid from your param value
    protected function _beforeLoad($modelId, $field = null)
    {
        parent::_beforeLoad($modelId, $field);

        $param = $this->request->getParam('ticket_id');

        $this->getSelect()
            ->joinLeft(
                ['ticket' => $this->getTable('mycompany_helpdesk_ticket')],
                'main_table.ticket_id = ticket.ticket_id',
                ['subject', 'customer_id'] // '*' define that you want all column of 2nd table.  you can define as ['column1','column2']
            )->joinLeft(
                ['thread' => $this->getTable('mycompany_helpdesk_thread')],
                'main_table.thread_id = thread.thread_id',
                ['message', 'created_at'] // '*' define that you want all column of 2nd table.  you can define as ['column1','column2']
            );

        $this->addFilterToMap('thread_id', 'main_table.thread_id');
        $this->addFilterToMap('ticket_id', 'main_table.ticket_id');


        if ($param != '') {
            $this->addFieldToFilter('ticket_id', $param);
        }

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getItems()
    {
        // TODO: Implement getItems() method.
    }
}
