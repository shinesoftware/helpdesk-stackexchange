<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Model;

use Magento\Framework\Api\ExtensionAttribute\JoinProcessorInterface;
use Magento\Framework\Exception\CouldNotSaveException;
use Mycompany\Helpdesk\Api\Data\ThreadSearchResultsInterfaceFactory;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Mycompany\Helpdesk\Model\ResourceModel\Thread as ResourceThread;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Reflection\DataObjectProcessor;
use Magento\Framework\Api\ExtensibleDataObjectConverter;
use Mycompany\Helpdesk\Model\ResourceModel\Thread\CollectionFactory as ThreadCollectionFactory;
use Mycompany\Helpdesk\Api\Data\ThreadInterfaceFactory;
use Mycompany\Helpdesk\Api\ThreadRepositoryInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class ThreadRepository implements ThreadRepositoryInterface
{

    protected $dataObjectHelper;

    protected $dataThreadFactory;

    private $storeManager;

    protected $searchResultsFactory;

    protected $dataObjectProcessor;

    protected $extensionAttributesJoinProcessor;

    private $collectionProcessor;

    protected $extensibleDataObjectConverter;
    protected $resource;

    protected $threadFactory;

    protected $threadCollectionFactory;


    /**
     * @param ResourceThread $resource
     * @param ThreadFactory $threadFactory
     * @param ThreadInterfaceFactory $dataThreadFactory
     * @param ThreadCollectionFactory $threadCollectionFactory
     * @param ThreadSearchResultsInterfaceFactory $searchResultsFactory
     * @param DataObjectHelper $dataObjectHelper
     * @param DataObjectProcessor $dataObjectProcessor
     * @param StoreManagerInterface $storeManager
     * @param CollectionProcessorInterface $collectionProcessor
     * @param JoinProcessorInterface $extensionAttributesJoinProcessor
     * @param ExtensibleDataObjectConverter $extensibleDataObjectConverter
     */
    public function __construct(
        ResourceThread $resource,
        ThreadFactory $threadFactory,
        ThreadInterfaceFactory $dataThreadFactory,
        ThreadCollectionFactory $threadCollectionFactory,
        ThreadSearchResultsInterfaceFactory $searchResultsFactory,
        DataObjectHelper $dataObjectHelper,
        DataObjectProcessor $dataObjectProcessor,
        StoreManagerInterface $storeManager,
        CollectionProcessorInterface $collectionProcessor,
        JoinProcessorInterface $extensionAttributesJoinProcessor,
        ExtensibleDataObjectConverter $extensibleDataObjectConverter
    ) {
        $this->resource = $resource;
        $this->threadFactory = $threadFactory;
        $this->threadCollectionFactory = $threadCollectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->dataObjectHelper = $dataObjectHelper;
        $this->dataThreadFactory = $dataThreadFactory;
        $this->dataObjectProcessor = $dataObjectProcessor;
        $this->storeManager = $storeManager;
        $this->collectionProcessor = $collectionProcessor;
        $this->extensionAttributesJoinProcessor = $extensionAttributesJoinProcessor;
        $this->extensibleDataObjectConverter = $extensibleDataObjectConverter;
    }

    /**
     * {@inheritdoc}
     */
    public function save(
        \Mycompany\Helpdesk\Api\Data\ThreadInterface $thread
    ) {
        /* if (empty($thread->getStoreId())) {
            $storeId = $this->storeManager->getStore()->getId();
            $thread->setStoreId($storeId);
        } */
        
        $threadData = $this->extensibleDataObjectConverter->toNestedArray(
            $thread,
            [],
            \Mycompany\Helpdesk\Api\Data\ThreadInterface::class
        );
        
        $threadModel = $this->threadFactory->create()->setData($threadData);
        
        try {
            $this->resource->save($threadModel);
        } catch (\Exception $exception) {
            throw new CouldNotSaveException(__(
                'Could not save the thread: %1',
                $exception->getMessage()
            ));
        }
        return $threadModel->getDataModel();
    }

    /**
     * {@inheritdoc}
     */
    public function get($threadId)
    {
        $thread = $this->threadFactory->create();
        $this->resource->load($thread, $threadId);
        if (!$thread->getId()) {
            throw new NoSuchEntityException(__('Thread with id "%1" does not exist.', $threadId));
        }
        return $thread->getDataModel();
    }

    /**
     * @param $ticketId
     * @param string $threadSort
     * @return mixed
     * @throws NoSuchEntityException
     */
    public function getThreadIdByTicketId($ticketId, $threadSort="ASC")
    {
        $collection = $this->threadCollectionFactory->create();
        $collection->addFieldToFilter('ticket_id' , ['eq' => $ticketId]);

        if (!$collection->count()) {
            throw new NoSuchEntityException(__('Thread record has been not found.', $ticketId));
        }

        return $collection->getFirstItem()->getThreadId();
    }

    /**
     * {@inheritdoc}
     */
    public function getList(
        \Magento\Framework\Api\SearchCriteriaInterface $criteria
    ) {
        $collection = $this->threadCollectionFactory->create();
        
        $this->extensionAttributesJoinProcessor->process(
            $collection,
            \Mycompany\Helpdesk\Api\Data\ThreadInterface::class
        );
        
        $this->collectionProcessor->process($criteria, $collection);
        
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        
        $items = [];
        foreach ($collection as $model) {
            $items[] = $model->getDataModel();
        }
        
        $searchResults->setItems($items);
        $searchResults->setTotalCount($collection->getSize());
        return $searchResults;
    }

    /**
     * {@inheritdoc}
     */
    public function delete(
        \Mycompany\Helpdesk\Api\Data\ThreadInterface $thread
    ) {
        try {
            $threadModel = $this->threadFactory->create();
            $this->resource->load($threadModel, $thread->getThreadId());
            $this->resource->delete($threadModel);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__(
                'Could not delete the Thread: %1',
                $exception->getMessage()
            ));
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function deleteById($threadId)
    {
        return $this->delete($this->get($threadId));
    }

}
