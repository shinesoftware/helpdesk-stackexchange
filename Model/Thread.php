<?php
/**
 * Shine Software Helpdesk System
 * Copyright (C) 2020 Shine Software
 * 
 * This file is part of Mycompany/Helpdesk.
 * 
 * Mycompany/Helpdesk is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

namespace Mycompany\Helpdesk\Model;

use Magento\Framework\Api\DataObjectHelper;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Model\AbstractModel;
use Magento\Framework\Model\Context;
use Magento\Framework\Registry;
use Magento\Framework\Stdlib\DateTime\DateTime;
use Mycompany\Helpdesk\Api\Data\ThreadInterface;
use Mycompany\Helpdesk\Api\Data\ThreadInterfaceFactory;
use Mycompany\Helpdesk\Api\Data\ThreadrelatedInterface;
use Mycompany\Helpdesk\Api\ThreadrelatedRepositoryInterface;
use Mycompany\Helpdesk\Api\ThreadRepositoryInterface;
use Mycompany\Helpdesk\Api\TicketRepositoryInterface;
use Mycompany\Helpdesk\Model\ResourceModel\Thread\Collection;

class Thread extends AbstractModel
{

    /**
     * @var DataObjectHelper
     */
    protected $dataObjectHelper;

    /**
     * @var string
     */
    protected $_eventPrefix = 'mycompany_helpdesk_thread';

    /**
     * @var ThreadInterfaceFactory
     */
    protected $thread;

    /**
     * @var DateTime
     */
    protected $date;

    /**
     * @var Status
     */
    protected $status;

    /**
     * @var Attachments
     */
    protected $attachments;

    /**
     * @var ThreadRepositoryInterface
     */
    protected $threadRepository;

    /**
     * @var ThreadrelatedRepositoryInterface
     */
    protected $threadRelatedRepository;

    /**
     * @var ThreadrelatedInterface
     */
    protected $threadRelated;

    /**
     * @var TicketRepositoryInterface|ResourceModel\Ticket
     */
    protected $ticket;


    /**
     * Thread constructor.
     * @param Context $context
     * @param Registry $registry
     * @param ThreadInterfaceFactory $threadInterfaceFactory
     * @param ThreadRepositoryInterface $threadRepository
     * @param ThreadrelatedRepositoryInterface $threadrelatedRepository
     * @param ThreadrelatedInterface $threadrelated
     * @param DataObjectHelper $dataObjectHelper
     * @param TicketRepositoryInterface $ticket
     * @param ResourceModel\Thread $resource
     * @param Collection $resourceCollection
     * @param Status $status
     * @param Attachments $attachments
     * @param DateTime $date
     * @param array $data
     */
    public function __construct(
        Context $context,
        Registry $registry,
        ThreadInterfaceFactory $threadInterfaceFactory,
        ThreadRepositoryInterface $threadRepository,
        ThreadrelatedRepositoryInterface $threadrelatedRepository,
        ThreadrelatedInterface $threadrelated,
        DataObjectHelper $dataObjectHelper,
        TicketRepositoryInterface $ticket,
        ResourceModel\Thread $resource,
        Collection $resourceCollection,
        DateTime $date,
        array $data = []
    ) {
        $this->date = $date;
        $this->ticket = $ticket;
        $this->thread = $threadInterfaceFactory;
        $this->threadRepository = $threadRepository;
        $this->threadRelatedRepository = $threadrelatedRepository;
        $this->threadRelated = $threadrelated;
        $this->dataObjectHelper = $dataObjectHelper;
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * Retrieve thread model with thread data
     * @return ThreadInterface
     */
    public function getDataModel()
    {
        $threadData = $this->getData();
        
        $threadDataObject = $this->thread->create();
        $this->dataObjectHelper->populateWithArray(
            $threadDataObject,
            $threadData,
            ThreadInterface::class
        );
        
        return $threadDataObject;
    }

}
